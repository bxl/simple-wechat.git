package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.testtool.TestConstants;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by buxianglong on 2017/9/6.
 */
public class AccessServiceTest{
    @Test
    public void testCheck(){
        WechatConstants wechatConstants = TestConstants.initWeixinConstants();
        String signature = "1f86fed5fdb6d11abae84a17cbf9a649b80f1ea6";
        String timestamp = "1504682805";
        String nonce = "2312247511";
        String echostr = "16518246000695947199";
        Assert.assertTrue(AccessService.check(wechatConstants, signature, timestamp, nonce, echostr));
    }
}
