package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.bean.usertag.CreateUserTagResultBean;
import com.adouteam.simple.wechat.bean.usertag.UserTags;
import com.adouteam.simple.wechat.testtool.TestConstants;
import org.apache.commons.lang3.StringUtils;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

/**
 * @author buxianglong
 * @date 2018/09/07
 **/
@Ignore
public class UserTagServiceTest{
    private static WechatConstants wechatConstants;

    @BeforeClass
    public static void init(){
        wechatConstants = TestConstants.initWeixinConstants();
    }

    @Test
    public void getAllUserTags() throws Exception{
        UserTags userTags = UserTagService.getAllUserTags(wechatConstants);
        assertNotNull(userTags);
        System.out.println(userTags);
    }

    @Test
    public void createUserTag() throws Exception{
        String nameOfNewUserTag = buildRandomUserTagName();
        CreateUserTagResultBean createUserTagResultBean = UserTagService.createUserTag(nameOfNewUserTag, wechatConstants);
        assertNotNull(createUserTagResultBean);
        assertEquals(nameOfNewUserTag, createUserTagResultBean.getTag().getName());
    }

    @Test
    public void updateUserTag() throws Exception{
        String nameOfNewUserTag = buildRandomUserTagName();
        CreateUserTagResultBean createUserTagResultBean = UserTagService.createUserTag(nameOfNewUserTag, wechatConstants);
        assertNotNull(createUserTagResultBean);
        assertEquals(nameOfNewUserTag, createUserTagResultBean.getTag().getName());
        int tagId = createUserTagResultBean.getTag().getId();
        assertTrue(tagId > 0);

        String nameOfUpdateUserTag = buildRandomUserTagName();
        System.out.println(nameOfUpdateUserTag);
        Boolean resultOfUpdateUserTag = UserTagService.updateUserTag(tagId, nameOfUpdateUserTag, wechatConstants);
        assertTrue(resultOfUpdateUserTag);
    }

    private String buildRandomUserTagName(){
        return StringUtils.substring("TEST" + UUID.randomUUID().toString(), 0, 30);
    }

}