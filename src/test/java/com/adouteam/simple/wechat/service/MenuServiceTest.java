package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.bean.Button;
import com.adouteam.simple.wechat.bean.FaButton;
import com.adouteam.simple.wechat.bean.Menu;
import com.adouteam.simple.wechat.bean.SubButton;
import com.adouteam.simple.wechat.testtool.TestConstants;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class MenuServiceTest{
    @Test
    public void testUpdateMenu(){
        WechatConstants wechatConstants = TestConstants.initWeixinConstants();
        List<Button> buttonList = new ArrayList<>();
        SubButton subButton1_1 = new SubButton("view", "测试1", "", "http://xmlx01.adouteam.com/1");
        SubButton subButton1_2 = new SubButton("view", "测试2", "", "http://xmlx01.adouteam.com/2");
        FaButton faButton1 = new FaButton("按钮1", new SubButton[]{subButton1_1, subButton1_2});
        SubButton subButton2_1 = new SubButton("view", "测试3", "", "http://xmlx01.adouteam.com/3");
        SubButton subButton2_2 = new SubButton("view", "测试4", "", "http://xmlx01.adouteam.com/4");
        SubButton subButton2_3 = new SubButton("view", "测试5", "", "http://xmlx01.adouteam.com/5");
        SubButton subButton2_4 = new SubButton("view", "测试6", "", "http://xmlx01.adouteam.com/6");
        SubButton subButton2_5 = new SubButton("view", "测试7", "", "http://xmlx01.adouteam.com/7");
        FaButton faButton2 = new FaButton("按钮2", new SubButton[]{subButton2_1, subButton2_2, subButton2_3, subButton2_4, subButton2_5});
        SubButton subButton3_1 = new SubButton("view", "测试8", "", "http://xmlx01.adouteam.com/8");
        SubButton subButton3_2 = new SubButton("view", "测试9", "", "http://xmlx01.adouteam.com/9");
        SubButton subButton3_3 = new SubButton("view", "测试10", "", "http://xmlx01.adouteam.com/10");
        SubButton subButton3_4 = new SubButton("click", "测试11", "ABOUT_COOPERATION", "http://xmlx01.adouteam.com/11");
        FaButton faButton3 = new FaButton("按钮3", new SubButton[]{subButton3_1, subButton3_2, subButton3_3, subButton3_4});
        buttonList.add(faButton1);
        buttonList.add(faButton2);
        buttonList.add(faButton3);
        Menu menu = new Menu(buttonList);
        Assert.assertTrue(MenuService.updateMenu(menu, wechatConstants));
    }

    @Test
    public void testFetchMenu(){
        WechatConstants wechatConstants = TestConstants.initWeixinConstants();
        String menuJson = "{\"menu\":{\"button\":[" +
                "{\"name\":\"按钮1\",\"sub_button\":[" +
                "{\"type\":\"view\",\"name\":\"测试1\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/1\",\"sub_button\":[]}," +
                "{\"type\":\"view\",\"name\":\"测试2\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/2\",\"sub_button\":[]}]}," +
                "{\"name\":\"按钮2\",\"sub_button\":[" +
                "{\"type\":\"view\",\"name\":\"测试3\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/3\",\"sub_button\":[]}," +
                "{\"type\":\"view\",\"name\":\"测试4\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/4\",\"sub_button\":[]}," +
                "{\"type\":\"view\",\"name\":\"测试5\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/5\",\"sub_button\":[]}," +
                "{\"type\":\"view\",\"name\":\"测试6\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/6\",\"sub_button\":[]}," +
                "{\"type\":\"view\",\"name\":\"测试7\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/7\",\"sub_button\":[]}]}," +
                "{\"name\":\"按钮3\",\"sub_button\":[" +
                "{\"type\":\"view\",\"name\":\"测试8\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/8\",\"sub_button\":[]}," +
                "{\"type\":\"view\",\"name\":\"测试9\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/9\",\"sub_button\":[]}," +
                "{\"type\":\"view\",\"name\":\"测试10\",\"url\":\"http:\\/\\/xmlx01.adouteam.com\\/10\",\"sub_button\":[]}," +
                "{\"type\":\"click\",\"name\":\"测试11\",\"key\":\"ABOUT_COOPERATION\",\"sub_button\":[]}]}]}}";
        Assert.assertEquals(menuJson, MenuService.fetchMenu(wechatConstants));
    }
}
