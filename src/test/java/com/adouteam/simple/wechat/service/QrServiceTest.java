package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.testtool.TestConstants;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author buxianglong
 * @date 2018/09/07
 **/
public class QrServiceTest{
    @Test
    public void getTempQrUrlOfSceneId() throws Exception{
        int sceneId = 1;
        Integer expireSeconds = 300;
        String tempQrUrl = QrService.getTempQrUrl(sceneId, expireSeconds, TestConstants.initWeixinConstants());
        assertTrue(StringUtils.isNotBlank(tempQrUrl));
        System.out.println(tempQrUrl);
    }

    @Test
    public void getTempQrUrlOfSceneStr() throws Exception{
        String sceneStr = "temp01";
        Integer expireSeconds = 300;
        String tempQrUrl = QrService.getTempQrUrl(sceneStr, expireSeconds, TestConstants.initWeixinConstants());
        assertTrue(StringUtils.isNotBlank(tempQrUrl));
        System.out.println(tempQrUrl);
    }

    @Test
    public void getPermanentQrUrlOfSceneId() throws Exception{
        int sceneId = 2;
        String permanentQrUrl = QrService.getPermanentQrUrl(sceneId, TestConstants.initWeixinConstants());
        assertTrue(StringUtils.isNotBlank(permanentQrUrl));
        String url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQHU7zoAAAAAAAAAASxodHRwOi8vd2VpeGluLnFxLmNvbS9xL0RrUHdNYkhselBXMUt1OVVzMjlnAAIE9mtHUwMEAAAAAA%3D%3D";
        assertEquals(url, permanentQrUrl);
    }

    @Test
    public void getPermanentQrUrlOfSceneStr() throws Exception{
        String sceneStr = "permanent01";
        String permanentQrUrl = QrService.getPermanentQrUrl(sceneStr, TestConstants.initWeixinConstants());
        assertTrue(StringUtils.isNotBlank(permanentQrUrl));
        String url = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket=gQF58TwAAAAAAAAAAS5odHRwOi8vd2VpeGluLnFxLmNvbS9xLzAySjFEY29sQUU5T1UxMDAwMHcwN2sAAgQDJpJbAwQAAAAA";
        assertEquals(url, permanentQrUrl);
    }

}