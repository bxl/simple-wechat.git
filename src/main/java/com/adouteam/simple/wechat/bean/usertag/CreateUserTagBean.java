package com.adouteam.simple.wechat.bean.usertag;

import java.util.Map;

public class CreateUserTagBean{
    private Map<String, String> tag;

    public Map<String, String> getTag(){
        return tag;
    }

    public void setTag(Map<String, String> tag){
        this.tag = tag;
    }
}
