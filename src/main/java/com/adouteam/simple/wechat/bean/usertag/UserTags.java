package com.adouteam.simple.wechat.bean.usertag;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class UserTags{
    @XStreamAlias("tags")
    private List<UserTag> tags;

    public List<UserTag> getTags(){
        return tags;
    }

    public void setTags(List<UserTag> tags){
        this.tags = tags;
    }

    @Override
    public String toString(){
        return "UserTags [tags=" + tags + "]";
    }
}