package com.adouteam.simple.wechat.bean.usertag;

public class CreateUserTagResultBean{
    public UserTag tag;

    public UserTag getTag(){
        return tag;
    }

    public void setTag(UserTag tag){
        this.tag = tag;
    }

    @Override
    public String toString(){
        return "CreateUserTagResultBean [tag=" + tag + "]";
    }
}
