package com.adouteam.simple.wechat.bean;

import java.util.List;

public class Menu{
    public Menu(List<Button> buttonList){
        this.button = buttonList;
    }

    private List<Button> button;

    public List<Button> getButton(){
        return button;
    }

    public void setButton(List<Button> button){
        this.button = button;
    }

}
