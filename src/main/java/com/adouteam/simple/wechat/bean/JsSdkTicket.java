package com.adouteam.simple.wechat.bean;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * Created by buxianglong on 2017/9/19.
 */
public class JsSdkTicket {
    @JSONField(name = "errcode")
    private int errorCode;
    @JSONField(name = "errmsg")
    private String errorMsg;
    @JSONField(name = "ticket")
    private String ticket;
    @JSONField(name = "expires_in")
    private int expiresIn;

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getTicket() {
        return ticket;
    }

    public void setTicket(String ticket) {
        this.ticket = ticket;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }
}
