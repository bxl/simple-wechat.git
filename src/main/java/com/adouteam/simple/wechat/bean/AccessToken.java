package com.adouteam.simple.wechat.bean;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 令牌
 *
 * @author buxianglong
 */
public class AccessToken{
    @JSONField(name = "errcode")
    private int errorCode;
    @JSONField(name = "errmsg")
    private String errorMsg;
    @JSONField(name = "access_token")
    private String accessToken;
    @JSONField(name = "expiresIn")
    private int expiresIn;

    public String getAccessToken(){
        return accessToken;
    }

    public void setAccessToken(String accessToken){
        this.accessToken = accessToken;
    }

    public int getExpiresIn(){
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn){
        this.expiresIn = expiresIn;
    }
}
