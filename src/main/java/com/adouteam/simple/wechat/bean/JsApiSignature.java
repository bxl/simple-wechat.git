package com.adouteam.simple.wechat.bean;

/**
 * Created by buxianglong on 2017/9/21.
 */
public class JsApiSignature {
    public JsApiSignature(String appId, String url, String nonceStr, Long timestamp, String signature) {
        this.appId = appId;
        this.url = url;
        this.nonceStr = nonceStr;
        this.timestamp = timestamp;
        this.signature = signature;
    }

    private String appId;
    private String url;
    private String nonceStr;
    private Long timestamp;
    private String signature;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNonceStr() {
        return nonceStr;
    }

    public void setNonceStr(String nonceStr) {
        this.nonceStr = nonceStr;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return "JsApiSignature{" +
                "appId='" + appId + '\'' +
                ", url='" + url + '\'' +
                ", nonceStr='" + nonceStr + '\'' +
                ", timestamp=" + timestamp +
                ", signature='" + signature + '\'' +
                '}';
    }
}
