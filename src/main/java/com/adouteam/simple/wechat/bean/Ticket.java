package com.adouteam.simple.wechat.bean;

public class Ticket{
    public String ticket;
    public int expire_seconds;

    public String getTicket(){
        return ticket;
    }

    public void setTicket(String ticket){
        this.ticket = ticket;
    }

    public int getExpire_seconds(){
        return expire_seconds;
    }

    public void setExpire_seconds(int expire_seconds){
        this.expire_seconds = expire_seconds;
    }

}
