package com.adouteam.simple.wechat.bean;

/**
 * @author buxianglong
 **/
public enum QrTypeEnum{
    QR_TEMP_INT(1, "QR_SCENE"), QR_TEMP_STR(2, "QR_STR_SCENE"),
    QR_PERMANET_INT(3, "QR_LIMIT_SCENE"), QR_PERMANET_STR(4, "QR_LIMIT_STR_SCENE");
    private int code;
    private String actionName;

    QrTypeEnum(int code, String actionName){
        this.code = code;
        this.actionName = actionName;
    }

    public int getCode(){
        return code;
    }

    public String getActionName(){
        return actionName;
    }
}
