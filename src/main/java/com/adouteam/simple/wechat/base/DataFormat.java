package com.adouteam.simple.wechat.base;

public class DataFormat{
    public DataFormat(String value){
        this.value = value;
    }

    public DataFormat(String value, String color){
        this.value = value;
        this.color = color;
    }

    private String value;
    private String color = "#173177";

    public String getValue(){
        return value;
    }

    public void setValue(String value){
        this.value = value;
    }

    public String getColor(){
        return color;
    }

    public void setColor(String color){
        this.color = color;
    }
}
