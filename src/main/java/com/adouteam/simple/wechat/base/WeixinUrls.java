package com.adouteam.simple.wechat.base;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.adouteam.simple.wechat.service.AccessTokenService;
import com.adouteam.simple.wechat.tool.PropertiesTool;

public class WeixinUrls{
    public static final String DOMAIN_NAME = PropertiesTool.getValue("DOMAIN_NAME");

    public static final String MENU_VIEW_REDIRECTB_BASE_URL = DOMAIN_NAME
            + "action/newWeixin/redirectToAppWithAppid?target_url=weixin/";

    public static final String BASE = "https://api.weixin.qq.com/cgi-bin/";
    public static final String BASE_OF_HTTP = "http://api.weixin.qq.com/cgi-bin/";

    public static final String ACCESS_TOKEN = BASE
            + "token?grant_type=client_credential&appid={APPID}&secret={APPSECRET}";

    public static final String WEIXIN_SERVER_IPS = BASE + "getcallbackip?access_token={ACCESS_TOKEN}";
    public static final String UPDATE_MENU = BASE + "menu/create?access_token={ACCESS_TOKEN}";
    public static final String FETCH_MENU = BASE + "menu/get?access_token={ACCESS_TOKEN}";

    public static final String AUTHORIZE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid={APPID}&redirect_uri={REDIRECT_URI}&response_type=code&scope={SCOPE}&state={STATE}#wechat_redirect";
    public static final String AUTHORIZE_ACCESSTOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid={APPID}&secret={APPSECRET}&code={CODE}&grant_type=authorization_code";
    public static final String AUTHORIZE_USERINFO_URL = "https://api.weixin.qq.com/sns/userinfo?access_token={ACCESS_TOKEN_USERINFO}&openid={OPENID}&lang=zh_CN";
    public static final String AUTHORIZE_USERINFO_UNIONID_URL = "https://api.weixin.qq.com/cgi-bin/user/info?access_token={ACCESS_TOKEN}&openid={OPENID}&lang=zh_CN";

    public static final String TEMPLATE_URL = BASE + "message/template/send?access_token={ACCESS_TOKEN}";

    public static final String QR_TICKET_URL = BASE + "qrcode/create?access_token={ACCESS_TOKEN}";
    public static final String QR_IMAGE_URL = "https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket={TICKET}";

    public static final String MEDIA_TEMP_UPLOAD_URL = BASE + "media/upload?access_token={ACCESS_TOKEN}&type={TYPE}";
    public static final String MEDIA_TEMP_GET_URL = BASE + "media/get?access_token={ACCESS_TOKEN}&media_id={MEDIA_ID}";
    public static final String MEDIA_TEMP_GET_URL_OF_HTTP = BASE_OF_HTTP
            + "media/get?access_token={ACCESS_TOKEN}&media_id={MEDIA_ID}";

    public static final String USER_TAG_GET_LIST_URL = BASE + "tags/get?access_token={ACCESS_TOKEN}";
    public static final String USER_TAG_CREATE_URL = BASE + "tags/create?access_token={ACCESS_TOKEN}";
    public static final String USER_TAG_UPDATE_URL = BASE + "tags/update?access_token={ACCESS_TOKEN}";
    public static final String USER_TAG_DELETE_URL = BASE + "tags/delete?access_token={ACCESS_TOKEN}";
    public static final String USER_TAG_GET_FANS_LIST_URL = BASE + "user/tag/get?access_token={ACCESS_TOKEN}";

    public static final String JS_SDK_GET_TICKET_URL = BASE + "ticket/getticket?access_token={ACCESS_TOKEN}&type=jsapi";

    public static final String WEB_LOGIN_OAUTH2_URL = "https://open.weixin.qq.com/connect/qrconnect?appid={APPID}&redirect_uri={REDIRECT_URI}&response_type=code&scope={SCOPE}&state={STATE}#wechat_redirect";

    public static String replace(String url, WechatConstants wechatConstants, Map<String, String> params){
        url = StringUtils.replace(url, "{APPID}", wechatConstants.getAppId());
        url = StringUtils.replace(url, "{APPSECRET}", wechatConstants.getAppSecret());
        if(StringUtils.contains(url, "{ACCESS_TOKEN}")){
            url = StringUtils.replace(url, "{ACCESS_TOKEN}", new AccessTokenService(wechatConstants).getAccessToken());
        }
        if(params == null || params.size() == 0){
            return url;
        }
        for(Map.Entry<String, String> entrySet : params.entrySet()){
            if(null == entrySet){
                continue;
            }
            url = StringUtils.replace(url, "{" + entrySet.getKey().toUpperCase() + "}", entrySet.getValue());
        }
        return url;
    }
}