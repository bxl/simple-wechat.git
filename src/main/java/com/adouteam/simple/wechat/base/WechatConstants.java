package com.adouteam.simple.wechat.base;

public class WechatConstants {

    public WechatConstants(String appId, String appSecret, String appToken, String appEncodingAesKey,
                           String appOriginId, String domainName, String cacheRegionOfAccessToken, String cacheKeyOfAccessToken,
                           String cacheRegionOfJsApiTicket, String cacheKeyOfJsApiTicket) {
        this.appId = appId;
        this.appSecret = appSecret;
        this.appToken = appToken;
        this.appEncodingAesKey = appEncodingAesKey;
        this.appOriginId = appOriginId;
        this.domainName = domainName;
        this.cacheRegionOfAccessToken = cacheRegionOfAccessToken;
        this.cacheKeyOfAccessToken = cacheKeyOfAccessToken;
        this.cacheRegionOfJsApiTicket = cacheRegionOfJsApiTicket;
        this.cacheKeyOfJsApiTicket = cacheKeyOfJsApiTicket;
    }

    private String appId;
    private String appSecret;
    private String appToken;
    private String appEncodingAesKey;
    private String appOriginId;

    private String domainName;

    private String cacheRegionOfAccessToken;
    private String cacheKeyOfAccessToken;

    private String cacheRegionOfJsApiTicket;
    private String cacheKeyOfJsApiTicket;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getAppToken() {
        return appToken;
    }

    public void setAppToken(String appToken) {
        this.appToken = appToken;
    }

    public String getAppEncodingAesKey() {
        return appEncodingAesKey;
    }

    public void setAppEncodingAesKey(String appEncodingAesKey) {
        this.appEncodingAesKey = appEncodingAesKey;
    }

    public String getAppOriginId() {
        return appOriginId;
    }

    public void setAppOriginId(String appOriginId) {
        this.appOriginId = appOriginId;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public String getCacheRegionOfAccessToken() {
        return cacheRegionOfAccessToken;
    }

    public void setCacheRegionOfAccessToken(String cacheRegionOfAccessToken) {
        this.cacheRegionOfAccessToken = cacheRegionOfAccessToken;
    }

    public String getCacheKeyOfAccessToken() {
        return cacheKeyOfAccessToken;
    }

    public void setCacheKeyOfAccessToken(String cacheKeyOfAccessToken) {
        this.cacheKeyOfAccessToken = cacheKeyOfAccessToken;
    }

    public String getCacheRegionOfJsApiTicket() {
        return cacheRegionOfJsApiTicket;
    }

    public void setCacheRegionOfJsApiTicket(String cacheRegionOfJsApiTicket) {
        this.cacheRegionOfJsApiTicket = cacheRegionOfJsApiTicket;
    }

    public String getCacheKeyOfJsApiTicket() {
        return cacheKeyOfJsApiTicket;
    }

    public void setCacheKeyOfJsApiTicket(String cacheKeyOfJsApiTicket) {
        this.cacheKeyOfJsApiTicket = cacheKeyOfJsApiTicket;
    }

    /**
     * 获取用户信息请求相关
     */
    public static final String USER_INFO_SCOPE_BASE = "snsapi_base";
    public static final String USER_INFO_SCOPE_USERINFO = "snsapi_userinfo";
    public static final String USER_INFO_STATE = "simple_weixin";
}
