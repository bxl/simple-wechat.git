package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 被动回复用户消息基础类
 * @author buxianglong
 **/
public class BaseMsgOfResponse{
    @XStreamAlias("ToUserName")
    @XStreamAnnotation.XStreamCDATA
    private String toUserName;
    @XStreamAlias("FromUserName")
    @XStreamAnnotation.XStreamCDATA
    private String fromUserName;
    @XStreamAlias("CreateTime")
    private String createTime;
    @XStreamAlias("MsgType")
    @XStreamAnnotation.XStreamCDATA
    private String msgType;

    public String getToUserName(){
        return toUserName;
    }

    public void setToUserName(String toUserName){
        this.toUserName = toUserName;
    }

    public String getFromUserName(){
        return fromUserName;
    }

    public void setFromUserName(String fromUserName){
        this.fromUserName = fromUserName;
    }

    public String getCreateTime(){
        return createTime;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getMsgType(){
        return msgType;
    }

    public void setMsgType(String msgType){
        this.msgType = msgType;
    }

    @Override
    public String toString(){
        return "BaseMsgOfResponse{" +
                "toUserName='" + toUserName + '\'' +
                ", fromUserName='" + fromUserName + '\'' +
                ", createTime='" + createTime + '\'' +
                ", msgType='" + msgType + '\'' +
                '}';
    }
}
