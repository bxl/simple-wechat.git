package com.adouteam.simple.wechat.msg;

/**
 * 微信消息基类
 *
 * @author buxianglong
 */
public class BaseMsg{
    private String toUserName;
    private String fromUserName;
    private String createTime;
    private String msgType;
    private long msgId;

    public String getToUserName(){
        return toUserName;
    }

    public void setToUserName(String toUserName){
        this.toUserName = toUserName;
    }

    public String getFromUserName(){
        return fromUserName;
    }

    public void setFromUserName(String fromUserName){
        this.fromUserName = fromUserName;
    }

    public String getCreateTime(){
        return createTime;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getMsgType(){
        return msgType;
    }

    public void setMsgType(String msgType){
        this.msgType = msgType;
    }

    public long getMsgId(){
        return msgId;
    }

    public void setMsgId(long msgId){
        this.msgId = msgId;
    }

    @Override
    public String toString(){
        return "BaseMsg{" +
                "toUserName='" + toUserName + '\'' +
                ", fromUserName='" + fromUserName + '\'' +
                ", createTime='" + createTime + '\'' +
                ", msgType='" + msgType + '\'' +
                ", msgId=" + msgId +
                '}';
    }
}
