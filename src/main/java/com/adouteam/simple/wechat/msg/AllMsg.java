package com.adouteam.simple.wechat.msg;

import com.alibaba.fastjson.annotation.JSONField;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 所有微信消息属性类
 *
 * @author buxianglong
 */
@XStreamAlias("xml")
public class AllMsg{
    @XStreamAlias("ToUserName")
    private String toUserName;
    @XStreamAlias("FromUserName")
    private String fromUserName;
    @XStreamAlias("CreateTime")
    private String createTime;
    @XStreamAlias("MsgType")
    private String msgType;
    @XStreamAlias("MsgId")
    private long msgId;
    /**
     * common message extend attribute
     */
    @XStreamAlias("Content")
    private String content;
    @XStreamAlias("PicUrl")
    private String picUrl;
    @XStreamAlias("MediaId")
    private String mediaId;
    @XStreamAlias("Format")
    private String format;
    @XStreamAlias("ThumbMediaId")
    private String thumbMediaId;
    @XStreamAlias("Location_X")
    private String locationX;
    @XStreamAlias("Location_Y")
    private String locationY;
    @XStreamAlias("Scale")
    private String scale;
    @XStreamAlias("Label")
    private String label;
    @XStreamAlias("Title")
    private String title;
    @XStreamAlias("Description")
    private String description;
    @XStreamAlias("Url")
    private String url;
    @XStreamAlias("Recognition")
    private String recognition;

    /**
     * 事件消息扩展部分
     */
    @XStreamAlias("Event")
    private String event;
    @XStreamAlias("EventKey")
    private String eventKey;
    @XStreamAlias("MenuId")
    private String menuId;
    @XStreamAlias("Ticket")
    private String ticket;
    @XStreamAlias("Latitude")
    private String latitude;
    @XStreamAlias("Longitude")
    private String longitude;
    @XStreamAlias("Precision")
    private String precision;

    public String getToUserName(){
        return toUserName;
    }

    public void setToUserName(String toUserName){
        this.toUserName = toUserName;
    }

    public String getFromUserName(){
        return fromUserName;
    }

    public void setFromUserName(String fromUserName){
        this.fromUserName = fromUserName;
    }

    public String getCreateTime(){
        return createTime;
    }

    public void setCreateTime(String createTime){
        this.createTime = createTime;
    }

    public String getMsgType(){
        return msgType;
    }

    public void setMsgType(String msgType){
        this.msgType = msgType;
    }

    public long getMsgId(){
        return msgId;
    }

    public void setMsgId(long msgId){
        this.msgId = msgId;
    }

    public String getContent(){
        return content;
    }

    public void setContent(String content){
        this.content = content;
    }

    public String getPicUrl(){
        return picUrl;
    }

    public void setPicUrl(String picUrl){
        this.picUrl = picUrl;
    }

    public String getMediaId(){
        return mediaId;
    }

    public void setMediaId(String mediaId){
        this.mediaId = mediaId;
    }

    public String getFormat(){
        return format;
    }

    public void setFormat(String format){
        this.format = format;
    }

    public String getThumbMediaId(){
        return thumbMediaId;
    }

    public void setThumbMediaId(String thumbMediaId){
        this.thumbMediaId = thumbMediaId;
    }

    public String getLocationX(){
        return locationX;
    }

    public void setLocationX(String locationX){
        this.locationX = locationX;
    }

    public String getLocationY(){
        return locationY;
    }

    public void setLocationY(String locationY){
        this.locationY = locationY;
    }

    public String getScale(){
        return scale;
    }

    public void setScale(String scale){
        this.scale = scale;
    }

    public String getLabel(){
        return label;
    }

    public void setLabel(String label){
        this.label = label;
    }

    public String getTitle(){
        return title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getDescription(){
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getUrl(){
        return url;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getRecognition(){
        return recognition;
    }

    public void setRecognition(String recognition){
        this.recognition = recognition;
    }

    public String getEvent(){
        return event;
    }

    public void setEvent(String event){
        this.event = event;
    }

    public String getEventKey(){
        return eventKey;
    }

    public void setEventKey(String eventKey){
        this.eventKey = eventKey;
    }

    public String getMenuId(){
        return menuId;
    }

    public void setMenuId(String menuId){
        this.menuId = menuId;
    }

    public String getTicket(){
        return ticket;
    }

    public void setTicket(String ticket){
        this.ticket = ticket;
    }

    public String getLatitude(){
        return latitude;
    }

    public void setLatitude(String latitude){
        this.latitude = latitude;
    }

    public String getLongitude(){
        return longitude;
    }

    public void setLongitude(String longitude){
        this.longitude = longitude;
    }

    public String getPrecision(){
        return precision;
    }

    public void setPrecision(String precision){
        this.precision = precision;
    }

    @Override
    public String toString(){
        return "AllMsg{" +
                "toUserName='" + toUserName + '\'' +
                ", fromUserName='" + fromUserName + '\'' +
                ", createTime='" + createTime + '\'' +
                ", msgType='" + msgType + '\'' +
                ", msgId=" + msgId +
                ", content='" + content + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", mediaId='" + mediaId + '\'' +
                ", format='" + format + '\'' +
                ", thumbMediaId='" + thumbMediaId + '\'' +
                ", locationX='" + locationX + '\'' +
                ", locationY='" + locationY + '\'' +
                ", scale='" + scale + '\'' +
                ", label='" + label + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", url='" + url + '\'' +
                ", recognition='" + recognition + '\'' +
                ", event='" + event + '\'' +
                ", eventKey='" + eventKey + '\'' +
                ", menuId='" + menuId + '\'' +
                ", ticket='" + ticket + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", precision='" + precision + '\'' +
                '}';
    }
}
