package com.adouteam.simple.wechat.msg;

import javax.servlet.http.HttpServletResponse;

/**
 * 用户消息接口
 *
 * @author buxianglong
 **/
public interface MessageAble{
    /**
     * 判断是否我自己
     * @param allMsg 综合消息体
     * @return true：是我自己；false：不是我自己
     */
    boolean isMe(AllMsg allMsg);

    /**
     * 复制属性
     *
     * @param allMsg 综合消息体
     */
    void copyProperties(AllMsg allMsg);

    /**
     * 主处理方法
     *
     * @param response 请求对象
     */
    void process(HttpServletResponse response);
}
