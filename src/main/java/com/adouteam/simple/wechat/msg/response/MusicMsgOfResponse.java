package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复音乐消息
 *
 * @author buxianglong
 */
public class MusicMsgOfResponse extends BaseMsgOfResponse{
    public static final String MSG_TYPE = "music";

    public MusicMsgOfResponse(){
        this.setMsgType(MSG_TYPE);
    }

    @XStreamAlias("Music")
    private Music music;

    public Music getMusic(){
        return music;
    }

    public void setMusic(Music music){
        this.music = music;
    }

    @Override
    public String toString(){
        return "MusicMsgOfResponse{" +
                "music=" + music +
                "} " + super.toString();
    }
}
