package com.adouteam.simple.wechat.msg.basic;

import com.adouteam.simple.wechat.msg.AllMsg;
import com.adouteam.simple.wechat.msg.BaseMsg;
import com.adouteam.simple.wechat.msg.MessageAble;
import com.adouteam.simple.wechat.service.ResponseService;

import javax.servlet.http.HttpServletResponse;

/**
 * 地理位置消息
 *
 * @author buxianglong
 */
public class LocationMsg extends BaseMsg implements MessageAble{
    private static final String MSG_TYPE = "location";
    private String locationX;
    private String locationY;
    private String scale;
    private String label;

    public String getLocationX(){
        return locationX;
    }

    public void setLocationX(String locationX){
        this.locationX = locationX;
    }

    public String getLocationY(){
        return locationY;
    }

    public void setLocationY(String locationY){
        this.locationY = locationY;
    }

    public String getScale(){
        return scale;
    }

    public void setScale(String scale){
        this.scale = scale;
    }

    public String getLabel(){
        return label;
    }

    public void setLabel(String label){
        this.label = label;
    }

    @Override
    public boolean isMe(AllMsg allMsg){
        if(null == allMsg){
            return false;
        }
        return MSG_TYPE.equalsIgnoreCase(allMsg.getMsgType());
    }

    @Override
    public void copyProperties(AllMsg allMsg){
        this.setFromUserName(allMsg.getFromUserName());
        this.setToUserName(allMsg.getToUserName());
        this.setCreateTime(allMsg.getCreateTime());
        this.setMsgType(allMsg.getMsgType());
        this.setMsgId(allMsg.getMsgId());
        this.setLocationX(allMsg.getLocationX());
        this.setLocationY(allMsg.getLocationY());
        this.setScale(allMsg.getScale());
        this.setLabel(allMsg.getLabel());
    }

    @Override
    public void process(HttpServletResponse response){
        ResponseService.responseBlank(response);
    }

    @Override
    public String toString(){
        return "LocationMsg{" +
                "locationX='" + locationX + '\'' +
                ", locationY='" + locationY + '\'' +
                ", scale='" + scale + '\'' +
                ", label='" + label + '\'' +
                "} " + super.toString();
    }
}
