package com.adouteam.simple.wechat.msg.basic;

import com.adouteam.simple.wechat.msg.AllMsg;
import com.adouteam.simple.wechat.msg.BaseMsg;
import com.adouteam.simple.wechat.msg.MessageAble;
import com.adouteam.simple.wechat.service.ResponseService;

import javax.servlet.http.HttpServletResponse;

/**
 * 图片消息
 *
 * @author buxianglong
 */
public class ImageMsg extends BaseMsg implements MessageAble{
    private static final String MSG_TYPE = "image";
    private String PicUrl;
    private String MediaId;

    public String getPicUrl(){
        return PicUrl;
    }

    public void setPicUrl(String picUrl){
        PicUrl = picUrl;
    }

    public String getMediaId(){
        return MediaId;
    }

    public void setMediaId(String mediaId){
        MediaId = mediaId;
    }

    @Override
    public boolean isMe(AllMsg allMsg){
        if(null == allMsg){
            return false;
        }
        return MSG_TYPE.equalsIgnoreCase(allMsg.getMsgType());
    }

    @Override
    public void copyProperties(AllMsg allMsg){
        this.setFromUserName(allMsg.getFromUserName());
        this.setToUserName(allMsg.getToUserName());
        this.setCreateTime(allMsg.getCreateTime());
        this.setMsgType(allMsg.getMsgType());
        this.setMsgId(allMsg.getMsgId());
        this.setPicUrl(allMsg.getPicUrl());
        this.setMediaId(allMsg.getMediaId());
    }

    @Override
    public void process(HttpServletResponse response){
        ResponseService.responseBlank(response);
    }

    @Override
    public String toString(){
        return "ImageMsg{" +
                "PicUrl='" + PicUrl + '\'' +
                ", MediaId='" + MediaId + '\'' +
                "} " + super.toString();
    }
}
