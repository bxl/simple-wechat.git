package com.adouteam.simple.wechat.msg.event;

import com.adouteam.simple.wechat.msg.AllMsg;
import com.adouteam.simple.wechat.msg.MessageAble;
import com.adouteam.simple.wechat.service.ResponseService;

import javax.servlet.http.HttpServletResponse;

/**
 * 上报地理位置事件
 *
 * @author buxianglong
 */
public class LocationEventMsg extends EventMsg implements MessageAble{
    private static final String EVENT = "LOCATION";
    private String latitude;
    private String longitude;
    private String precision;

    public String getLatitude(){
        return latitude;
    }

    public void setLatitude(String latitude){
        this.latitude = latitude;
    }

    public String getLongitude(){
        return longitude;
    }

    public void setLongitude(String longitude){
        this.longitude = longitude;
    }

    public String getPrecision(){
        return precision;
    }

    public void setPrecision(String precision){
        this.precision = precision;
    }

    @Override
    public boolean isMe(AllMsg allMsg){
        if(null == allMsg){
            return false;
        }
        return MSG_TYPE.equalsIgnoreCase(allMsg.getMsgType()) && EVENT.equalsIgnoreCase(allMsg.getEvent());
    }

    @Override
    public void copyProperties(AllMsg allMsg){
        this.setFromUserName(allMsg.getFromUserName());
        this.setToUserName(allMsg.getToUserName());
        this.setCreateTime(allMsg.getCreateTime());
        this.setMsgType(allMsg.getMsgType());
        this.setMsgId(allMsg.getMsgId());
        this.setLatitude(allMsg.getLatitude());
        this.setLongitude(allMsg.getLongitude());
        this.setPrecision(allMsg.getPrecision());
    }

    @Override
    public void process(HttpServletResponse response){
        ResponseService.responseBlank(response);
    }

    @Override
    public String toString(){
        return "LocationEventMsg{" +
                "latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", precision='" + precision + '\'' +
                "} " + super.toString();
    }
}
