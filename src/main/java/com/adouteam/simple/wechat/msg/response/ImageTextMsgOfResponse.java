package com.adouteam.simple.wechat.msg.response;

import com.adouteam.simple.wechat.tool.XStreamAnnotation;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.List;

/**
 * 回复图文消息
 *
 * @author buxianglong
 */
public class ImageTextMsgOfResponse extends BaseMsgOfResponse{
    public static final String MSG_TYPE = "news";

    public ImageTextMsgOfResponse(){
        this.setMsgType(MSG_TYPE);
    }

    @XStreamAlias("ArticleCount")
    private int articleCount;
    @XStreamAlias("Articles")
    @XStreamAnnotation.XStreamCDATA
    private List<Article> articles;

    public int getArticleCount(){
        return articleCount;
    }

    public void setArticleCount(int articleCount){
        this.articleCount = articleCount;
    }

    public List<Article> getArticles(){
        return articles;
    }

    public void setArticles(List<Article> articles){
        this.articles = articles;
    }

    @Override
    public String toString(){
        return "ImageTextMsgOfResponse{" +
                "articleCount=" + articleCount +
                ", articles=" + articles +
                "} " + super.toString();
    }
}
