package com.adouteam.simple.wechat.service;

import com.adouteam.simple.wechat.msg.WechatMsgProcessor;
import com.adouteam.simple.wechat.msg.AllMsg;

import javax.servlet.http.HttpServletResponse;

/**
 * 响应用户发送消息服务
 *
 * @author buxianglong
 **/
public class MessageService{
    /**
     * 处理用户发送信息，按照预定义返回实时响应
     *
     * @param response  http请求对象
     * @param allMsg 用户发送的原始信息对象
     */
    public static void processMessage(HttpServletResponse response, AllMsg allMsg){
        WechatMsgProcessor.process(allMsg, response);
    }
}
