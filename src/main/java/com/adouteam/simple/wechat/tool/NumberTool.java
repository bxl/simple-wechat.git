package com.adouteam.simple.wechat.tool;

import java.util.Random;

public class NumberTool{
    private final static char[] NUMBERS = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    public static String getRandomNumberStr(int length){
        StringBuilder retStr = new StringBuilder("");
        Random ran = new Random();
        for(int i = 0; i < length; i++){
            retStr.append(NUMBERS[ran.nextInt(NUMBERS.length)]);
        }
        return retStr.toString();
    }
}
