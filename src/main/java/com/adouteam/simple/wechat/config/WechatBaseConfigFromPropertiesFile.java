package com.adouteam.simple.wechat.config;

import com.adouteam.simple.wechat.base.WechatConstants;
import com.adouteam.simple.wechat.tool.PropertiesTool;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * 从配置文件读取微信配置信息
 *
 * @author buxianglong
 */
public class WechatBaseConfigFromPropertiesFile implements WechatBaseConfig{
    private static final Logger logger = LoggerFactory.getLogger(WechatBaseConfigFromPropertiesFile.class);
    public static final WechatBaseConfig INSTANCE = new WechatBaseConfigFromPropertiesFile();

    /**
     * 缓存相关
     */
    public static final String DEFAULT_CACHE_REGION_ACCESS_TOKEN = "10min";
    public static final String DEFAULT_CACHE_KEY_ACCESS_TOKEN = "#ACCESS#TOKEN#";

    public static final String DEFAULT_CACHE_REGION_JS_API_TICKET = "10min";
    public static final String DEFAULT_CACHE_KEY_JS_API_TICKET = "#JSAPI#TICKET#";

    private final List<String> appOriginList = new ArrayList();
    private final Map<String, WechatConstants> WechatConstantsList = new HashMap<>();

    private WechatBaseConfigFromPropertiesFile(){
        init();
    }

    private void init(){
        logger.info("WechatBaseConfigFromPropertiesFile initing...");
        Set<Object> keySets = PropertiesTool.keySet();
        logger.info("keySets:" + keySets);
        List<String> prefixes = new ArrayList<>();
        for(Object keySet : keySets){
            String keySetString = StringUtils.trimToEmpty(keySet.toString());
            if(StringUtils.endsWith(keySetString, "APP_ID")){
                String prefixString = StringUtils.substringBefore(keySetString, "APP_ID");
                if(StringUtils.isNotBlank(prefixString)){
                    prefixes.add(prefixString);
                }
            }
        }
        if(prefixes.size() > 0){
            for(int i = 0; i < prefixes.size(); i++){
                String prefix = prefixes.get(i);
                logger.info("prefix:" + prefix);
                String appId = StringUtils
                        .trimToEmpty(PropertiesTool.getValue(prefix + "APP_ID"));
                String appSecret = PropertiesTool.getValue(prefix + "APP_SECRET");
                String appToken = PropertiesTool.getValue(prefix + "APP_TOKEN");
                String appEncodingAesKey = PropertiesTool.getValue(prefix + "APP_ENCODINGAESKEY");
                String appOriginId = PropertiesTool.getValue(prefix + "APP_ORIGIN_ID");
                String domainName = PropertiesTool.getValue(prefix + "DOMAIN_NAME");
                String cacheRegionOfAccessToken = DEFAULT_CACHE_REGION_ACCESS_TOKEN;
                String cacheKeyOfAccessToken = DEFAULT_CACHE_KEY_ACCESS_TOKEN + appOriginId;
                String cacheRegionOfJsApiTicket = DEFAULT_CACHE_REGION_JS_API_TICKET;
                String cacheKeyOfJsApiTicket = DEFAULT_CACHE_KEY_JS_API_TICKET + appOriginId;
                WechatConstants wechatConstants = new WechatConstants(appId, appSecret, appToken, appEncodingAesKey,
                        appOriginId, domainName, cacheRegionOfAccessToken, cacheKeyOfAccessToken, cacheRegionOfJsApiTicket, cacheKeyOfJsApiTicket);
                WechatConstantsList.put(appOriginId, wechatConstants);
                appOriginList.add(appOriginId);
            }
        }
    }

    @Override
    public List<String> getAppOriginIds(){
        return Collections.unmodifiableList(appOriginList);
    }

    @Override
    public WechatConstants getByAppOriginId(String originId){
        return WechatConstantsList.get(originId);
    }
}
